#
# Extend table structure for table 'tx_mooxnews_domain_model_news'
#
CREATE TABLE tx_mooxnews_domain_model_news (
	moox_news_geoinfo_venue varchar(255) DEFAULT NULL,
	moox_news_geoinfo_street varchar(255) DEFAULT NULL,
	moox_news_geoinfo_zip varchar(20) DEFAULT NULL,
	moox_news_geoinfo_city varchar(255) DEFAULT NULL,
	moox_news_geoinfo_country varchar(255) DEFAULT NULL,
	moox_news_geoinfo_latitude varchar(50) DEFAULT NULL,
	moox_news_geoinfo_longitude varchar(50) DEFAULT NULL,
	moox_news_geoinfo_elevation varchar(50) DEFAULT NULL,
);