<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package moox_news_geoinfo
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_MooxNewsGeoinfo_Domain_Model_News extends Tx_MooxNews_Domain_Model_News {
	
	/**
	 * @var string
	 */
	protected $mooxNewsGeoinfoVenue;
	
	/**
	 * @var string
	 */
	protected $mooxNewsGeoinfoStreet;
	
	/**
	 * @var string
	 */
	protected $mooxNewsGeoinfoZip;
	
	/**
	 * @var string
	 */
	protected $mooxNewsGeoinfoCity;
	
	/**
	 * @var string
	 */
	protected $mooxNewsGeoinfoCountry;
	
	/**
	 * @var string
	 */
	protected $mooxNewsGeoinfoLatitude;
	
	/**
	 * @var string
	 */
	protected $mooxNewsGeoinfoLongitude;
	
	/**
	 * @var string
	 */
	protected $mooxNewsGeoinfoElevation;
	
	/**
	 * @var string
	 */
	protected $mooxNewsGeoinfoLatLngQuery;
	
	/**
	 * @var string
	 */
	protected $mooxNewsGeoinfoAddressQuery;
	
	/**
	 * @var boolean
	 */
	protected $mooxNewsGeoinfoMapAvailable;
	
	/**
	 * Get venue
	 *
	 * @return string
	 */
	public function getMooxNewsGeoinfoVenue($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxNewsGeoinfoVenue!=""){
				return $this->mooxNewsGeoinfoVenue;
			} else {
				return "";
			}
		} else {
			return $this->mooxNewsGeoinfoVenue;
		}
	}

	/**
	 * Set venue
	 *
	 * @param string $mooxNewsGeoinfoVenue venue
	 * @return void
	 */
	public function setMooxNewsGeoinfoVenue($mooxNewsGeoinfoVenue) {
		$this->mooxNewsGeoinfoVenue = $mooxNewsGeoinfoVenue;
	}
	
	/**
	 * Get street
	 *
	 * @return string
	 */
	public function getMooxNewsGeoinfoStreet($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxNewsGeoinfoStreet!=""){
				return $this->mooxNewsGeoinfoStreet;
			} else {
				return "";
			}
		} else {
			return $this->mooxNewsGeoinfoStreet;
		}
	}

	/**
	 * Set street
	 *
	 * @param string $mooxNewsGeoinfoStreet street
	 * @return void
	 */
	public function setMooxNewsGeoinfoStreet($mooxNewsGeoinfoStreet) {
		$this->mooxNewsGeoinfoStreet = $mooxNewsGeoinfoStreet;
	}
	
	/**
	 * Get zip
	 *
	 * @return string
	 */
	public function getMooxNewsGeoinfoZip($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxNewsGeoinfoZip!=""){
				return $this->mooxNewsGeoinfoZip;
			} else {
				return "";
			}
		} else {
			return $this->mooxNewsGeoinfoZip;
		}
	}

	/**
	 * Set zip
	 *
	 * @param string $mooxNewsGeoinfoZip zip
	 * @return void
	 */
	public function setMooxNewsGeoinfoZip($mooxNewsGeoinfoZip) {
		$this->mooxNewsGeoinfoZip = $mooxNewsGeoinfoZip;
	}
	
	/**
	 * Get city
	 *
	 * @return string
	 */
	public function getMooxNewsGeoinfoCity($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxNewsGeoinfoCity!="" || $this->mooxNewsGeoinfoZip!=""){
				$previewTxt = $this->mooxNewsGeoinfoZip;
				if($previewTxt!=""){
					$previewTxt .= " ".$this->mooxNewsGeoinfoCity;					
				} else {
					$previewTxt = $this->mooxNewsGeoinfoCity;
				}
				return $previewTxt;
			} else {
				return "";
			}
		} else {
			return $this->mooxNewsGeoinfoCity;
		}
	}

	/**
	 * Set city
	 *
	 * @param string $mooxNewsGeoinfoCity city
	 * @return void
	 */
	public function setMooxNewsGeoinfoCity($mooxNewsGeoinfoCity) {
		$this->mooxNewsGeoinfoCity = $mooxNewsGeoinfoCity;
	}
	
	/**
	 * Get country
	 *
	 * @return string
	 */
	public function getMooxNewsGeoinfoCountry($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxNewsGeoinfoCountry!=""){
				return $this->mooxNewsGeoinfoCountry;
			} else {
				return "";
			}
		} else {
			return $this->mooxNewsGeoinfoCountry;
		}
	}

	/**
	 * Set country
	 *
	 * @param string $mooxNewsGeoinfoCountry country
	 * @return void
	 */
	public function setMooxNewsGeoinfoCountry($mooxNewsGeoinfoCountry) {
		$this->mooxNewsGeoinfoCountry = $mooxNewsGeoinfoCountry;
	}
	
	/**
	 * Get latitude
	 *
	 * @return string
	 */
	public function getMooxNewsGeoinfoLatitude($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxNewsGeoinfoLatitude!=""){
				$DMS = $this->DEC2DMS($this->mooxNewsGeoinfoLatitude);
				return $DMS['deg']."° ".$DMS['min']."' ".$DMS['sec']."\" [".$this->mooxNewsGeoinfoLatitude."]";
			} else {
				return "";
			}
		} else {
			return $this->mooxNewsGeoinfoLatitude;
		}
	}

	/**
	 * Set latitude
	 *
	 * @param string $mooxNewsGeoinfoLatitude latitude
	 * @return void
	 */
	public function setMooxNewsGeoinfoLatitude($mooxNewsGeoinfoLatitude) {
		$this->mooxNewsGeoinfoLatitude = $mooxNewsGeoinfoLatitude;
	}
	
	/**
	 * Get longitude
	 *
	 * @return string
	 */
	public function getMooxNewsGeoinfoLongitude($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxNewsGeoinfoLongitude!=""){
				$DMS = $this->DEC2DMS($this->mooxNewsGeoinfoLongitude);
				return $DMS['deg']."° ".$DMS['min']."' ".$DMS['sec']."\" [".$this->mooxNewsGeoinfoLongitude."]";
			} else {
				return "";
			}
		} else {
			return $this->mooxNewsGeoinfoLongitude;
		}
	}

	/**
	 * Set longitude
	 *
	 * @param string $mooxNewsGeoinfoLongitude longitude
	 * @return void
	 */
	public function setMooxNewsGeoinfoLongitude($mooxNewsGeoinfoLongitude) {
		$this->mooxNewsGeoinfoLongitude = $mooxNewsGeoinfoLongitude;
	}
	
	/**
	 * Get elevation
	 *
	 * @return string
	 */
	public function getMooxNewsGeoinfoElevation($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxNewsGeoinfoElevation!=""){
				return $this->mooxNewsGeoinfoElevation;
			} else {
				return "";
			}
		} else {
			return $this->mooxNewsGeoinfoElevation;
		}
	}

	/**
	 * Set elevation
	 *
	 * @param string $mooxNewsGeoinfoElevation elevation
	 * @return void
	 */
	public function setMooxNewsGeoinfoElevation($mooxNewsGeoinfoElevation) {
		$this->mooxNewsGeoinfoElevation = $mooxNewsGeoinfoElevation;
	}
	
	/**
	 * Get lat lng query
	 *
	 * @return string
	 */
	public function getMooxNewsGeoinfoLatLngQuery() {
		$latLngQuery = "";		
		if($this->mooxNewsGeoinfoLatitude!="" && $this->mooxNewsGeoinfoLongitude!=""){
			$latLngQuery = $this->mooxNewsGeoinfoLatitude.",".$this->mooxNewsGeoinfoLongitude;
		}
		return $latLngQuery;
		
	}
	
	/**
	 * Get address query
	 *
	 * @return string
	 */
	public function getMooxNewsGeoinfoAddressQuery() {
		$addressQuery = "";		
		if($this->mooxNewsGeoinfoStreet!="" && ($this->mooxNewsGeoinfoZip!="" || $this->mooxNewsGeoinfoCity!="")){
			if($this->mooxNewsGeoinfoStreet!=""){
				$addressQuery = $this->mooxNewsGeoinfoStreet.",";
			}
			if($this->mooxNewsGeoinfoZip!=""){
				$addressQuery .= $this->mooxNewsGeoinfoZip." ";
			}
			if($this->mooxNewsGeoinfoCity!=""){
				$addressQuery .= $this->mooxNewsGeoinfoCity;
			}
			if($this->mooxNewsGeoinfoCountry!=""){
				$addressQuery .= ",".$this->mooxNewsGeoinfoCountry;
			}
		}
		return $addressQuery;
		
	}
	
	/**
	 * Get map available
	 *
	 * @return boolean
	 */
	public function getMooxNewsGeoinfoMapAvailable() {			
		if($this->mooxNewsGeoinfoLatLngQuery!="" || $this->mooxNewsGeoinfoAddressQuery!=""){
			return true;
		} else {
			return false;
		}		
	}
	
	/**
	 * Converts decimal longitude / latitude to DMS
	 * (Degrees/minutes/seconds) 
	 * @param float $dec dec
	 * @return array
	 */
	function DEC2DMS($dec){

		$vars = explode(".",$dec);
		$deg = $vars[0];
		$tempma = "0.".$vars[1];

		$tempma = $tempma * 3600;
		$min = floor($tempma / 60);
		$sec = $tempma - ($min*60);

		return array("deg"=>$deg,"min"=>$min,"sec"=>$sec);
	}    

}
