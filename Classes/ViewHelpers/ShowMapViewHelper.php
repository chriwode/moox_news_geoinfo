<?php
namespace DCNGmbH\MooxNewsGeoinfo\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
 
/**
 * ### ShowMap
 *
 * @package moox_news_geoinfo
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @subpackage ViewHelpers
 */
class ShowMapViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
	
	/**
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerArgument('newsItem', 'object', 'News item', TRUE);
		$this->registerArgument('elementid', 'string', 'Element id', TRUE);
		$this->registerArgument('class', 'string', 'CSS class', FALSE, FALSE);
		$this->registerArgument('width', 'integer', 'Map width', TRUE);
		$this->registerArgument('height', 'integer', 'Map height', TRUE);
	}
	
	/**
	 * @return string
	 */
	public function render() {
		
		/*
		google.maps.event.addListener(marker, "click", function() {
			infowindow.open(map, marker);
		});
		*/
		
		$this->arguments['elementid'] = trim($this->arguments['elementid']);				
		$this->arguments['class'] = trim($this->arguments['class']);
		
		if($this->arguments['width']!="" || $this->arguments['height']!=""){
			$style = 'style="';
			if($this->arguments['width']!=""){
				$style .= 'width:'.$this->arguments['width'].';';
			}
			
			if($this->arguments['height']!=""){
				$style .= 'height:'.$this->arguments['height'].';';
			}
			$style .= '" ';
		}
		
		$fulltitle = "";
		$title = "";
		if($this->arguments['newsItem']->getMooxNewsGeoinfoVenue()!=""){
			$fulltitle .= "<b>".$this->arguments['newsItem']->getMooxNewsGeoinfoVenue()."</b><br>";	
			$title 		= $this->arguments['newsItem']->getMooxNewsGeoinfoVenue();
		}
		if($this->arguments['newsItem']->getMooxNewsGeoinfoStreet()!=""){
			$fulltitle .= $this->arguments['newsItem']->getMooxNewsGeoinfoStreet()."<br>";
			if($this->arguments['newsItem']->getMooxNewsGeoinfoVenue()==""){
				$title .= $this->arguments['newsItem']->getMooxNewsGeoinfoStreet().", ";
			}
		}
		if($this->arguments['newsItem']->getMooxNewsGeoinfoZip()!=""){
			$fulltitle .= $this->arguments['newsItem']->getMooxNewsGeoinfoZip()." ";
			if($this->arguments['newsItem']->getMooxNewsGeoinfoVenue()==""){
				$title .= $this->arguments['newsItem']->getMooxNewsGeoinfoZip()." ";
			}
		}
		$fulltitle .= $this->arguments['newsItem']->getMooxNewsGeoinfoCity();
		if($this->arguments['newsItem']->getMooxNewsGeoinfoVenue()==""){
			$title .= $this->arguments['newsItem']->getMooxNewsGeoinfoCity();
		}
		
		if($fulltitle==""){
			$fulltitle = $this->arguments['newsItem']->getMooxNewsGeoinfoLatLngQuery();
		}
		if($title==""){
			$title = $this->arguments['newsItem']->getMooxNewsGeoinfoLatLngQuery();
		}
		
		if($this->arguments['newsItem']->getMooxNewsGeoinfoLatLngQuery()!=""){
			$geocoding = '	var lat = '.$this->arguments['newsItem']->getMooxNewsGeoinfoLatitude().';
							var lng = '.$this->arguments['newsItem']->getMooxNewsGeoinfoLongitude().';';
		} elseif($this->arguments['newsItem']->getMooxNewsGeoinfoAddressQuery()!=""){
			$geocoding = '	geocoder.geocode({
								address: "'.$this->arguments['newsItem']->getMooxNewsGeoinfoAddressQuery().'"
							}, function(locResult) {
								lat = locResult[0].geometry.location.lat();
								lng = locResult[0].geometry.location.lng();
							});';
		}
		
		$map  = '<div '.(($this->arguments['class']!="")?'class="'.$this->arguments['class'].'" ':'').$style.'id="'.$this->arguments['elementid'].'"></div>';
		$map .= '<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
				<script type="text/javascript">
					<!--
					var geocoder = new google.maps.Geocoder();

					function InitGoogleMap() {
						'.$geocoding.'
						var latlng = new google.maps.LatLng(lat,lng);
						var options = {
							zoom: 16,
							center: latlng,
							panControl: true,
							zoomControl: true,
							zoomControlOptions: {
								style: google.maps.ZoomControlStyle.DEFAULT
							},

							mapTypeControl: true,
							mapTypeControlOptions: {
							  style: google.maps.MapTypeControlStyle.DEFAULT
							},
							streetViewControl: false,
							mapTypeId: google.maps.MapTypeId.ROADMAP
							}
						var map = new google.maps.Map(document.getElementById("'.$this->arguments['elementid'].'"), options);
						var marker = new google.maps.Marker({
							position: latlng,
							map: map,
							title:"'.$this->arguments['newsItem']->getMooxNewsGeoinfoVenue().'"
						});
						var infowindow = new google.maps.InfoWindow({
							content: "'.$fulltitle.'"
						});
						infowindow.open(map, marker);
					}
					google.maps.event.addDomListener(window,"load",InitGoogleMap);
					-->
				</script>
				';
		if($geocoding){
			return $map;
		} else {
			return '';
		}
	}
}