<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Hook into tcemain which is used to show preview of news item
 *
 * @package TYPO3
 * @subpackage tx_mooxnewsgeoinfo
 */
class Tx_MooxNewsGeoinfo_Hooks_Tcemain extends Tx_MooxNews_Hooks_Tcemain {

	/**
	 * do change item after saving
	 *
	 * @param object $item
	 * @return void
	 */
	public function external_MooxNewsGeoinfo($item) {
		$return = false;
		if($item->getMooxNewsGeoinfoLatitude()!=""){
			$latitude = $item->getMooxNewsGeoinfoLatitude();
			$latitude = str_replace(",",".",$latitude);			
			$item->setMooxNewsGeoinfoLatitude($latitude);
			$return = true;
		}
		if($item->getMooxNewsGeoinfoLongitude()!=""){
			$longitude = $item->getMooxNewsGeoinfoLongitude();
			$longitude = str_replace(",",".",$longitude);
			$item->setMooxNewsGeoinfoLongitude($longitude);
			$return = true;
		}
		return $return;
	}
}