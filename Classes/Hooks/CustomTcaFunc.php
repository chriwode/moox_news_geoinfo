<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package moox_news_geoinfo
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_MooxNewsGeoinfo_Hooks_CustomTcaFunc {
		
	/**
	 * generate country form
	 *
	 * @param array $PA
	 * @param array $fobj
	 * @return string $tcaForm
	 */
	function generateTcaCountryField(&$PA, &$fobj)    {			
		$query = "";
		if($PA['row']['moox_news_geoinfo_street']!=""){
			$query = $PA['row']['moox_news_geoinfo_street'].",";
		}
		if($PA['row']['moox_news_geoinfo_zip']!=""){
			$query .= $PA['row']['moox_news_geoinfo_zip']." ";
		}
		if($PA['row']['moox_news_geoinfo_city']!=""){
			$query .= $PA['row']['moox_news_geoinfo_city'];
		}
		if($PA['row']['moox_news_geoinfo_country']!=""){
			$query .= ",".$PA['row']['moox_news_geoinfo_country'];
		}
		
		$title = "";
		if($PA['row']['moox_news_geoinfo_venue']!=""){
			$title .= "<b>".$PA['row']['moox_news_geoinfo_venue']."</b><br>";			
		}
		if($PA['row']['moox_news_geoinfo_street']!=""){
			$title .= $PA['row']['moox_news_geoinfo_street']."<br>";
		}
		if($PA['row']['moox_news_geoinfo_zip']!=""){
			$title .= $PA['row']['moox_news_geoinfo_zip']." ";
		}
		$title .= $PA['row']['moox_news_geoinfo_city'];
				
		$formField .= '<div><input style="width:210px" type="text" name="' . $PA['itemFormElName'] . '"';
        $formField .= ' value="' . htmlspecialchars($PA['itemFormElValue']) . '"';
        $formField .= ' onchange="' . htmlspecialchars(implode('', $PA['fieldChangeFunc'])) . '"';
        $formField .= $PA['onFocus'];
        $formField .= ' /><button type="button" onclick="gmegInitializeMap();return false" title="Vorschau-Karte und Geo-Koordinaten aktualisieren">Vorschau-Karte und Geo-Koordinaten aktualisieren</button></div>';
		
		$map = '
		<div id="moox_news_geoinfo_preview_header" style="margin-top: 10px;margin-bottom: 10px; display: none"><strong>Vorschau auf Karte</strong></div>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
		<div style="width:540px;height:300px;display: none" id="gmeg_map_canvas"></div>
		<script type="text/javascript" >
			var gmegMap, gmegMarker, gmegInfoWindow, gmegLatLng;
			var geocoder = new google.maps.Geocoder();
			
			function gmegInitializeMap(){
				
				
				
				query = "";
				if(TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_street\']" ).val()!=""){
					query = TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_street\']" ).val() + ",";
				}
				if(TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_zip\']" ).val()!=""){
					query = query + TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_zip\']" ).val() + " ";
				}
				if(TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_city\']" ).val()!=""){
					query = query + TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_city\']" ).val();
				}
				if(TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_country\']" ).val()!=""){
					query = query + "," + TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_country\']" ).val();
				}
				
				if(query!="" || (TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_latitude\']" ).val()!="" && TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_longitude\']" ).val()!="")){
					TYPO3.jQuery("#moox_news_geoinfo_preview_header").show();
					TYPO3.jQuery("#gmeg_map_canvas").show();
				}
				
				title = "";
				if(TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_venue\']" ).val()!=""){
					title = title + "<b>" + TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_venue\']" ).val() + "</b><br>";			
				}
				if(TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_street\']" ).val()!=""){
					title = title + TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_street\']" ).val() + "<br>";
				}
				if(TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_zip\']" ).val()!=""){
					title = title + TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_zip\']" ).val() + " ";
				}
				title = title + TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_city\']" ).val();
				
				if(TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_latitude\']" ).val()!="" && TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_longitude\']" ).val()!=""){				
					lat = TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_latitude\']" ).val();
					lng = TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_longitude\']" ).val();
					gmegLatLng = 		new google.maps.LatLng(lat,lng);
					gmegMap = 			new google.maps.Map(
											document.getElementById("gmeg_map_canvas"),
											{zoom:15,center:gmegLatLng,mapTypeId:google.maps.MapTypeId.ROADMAP}
										);
					gmegMarker = 		new google.maps.Marker(
											{map:gmegMap,position:gmegLatLng}
										);
					gmegInfoWindow = 	new google.maps.InfoWindow(
											{content: title}
										);
					gmegInfoWindow.open(gmegMap,gmegMarker);
					if(TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_latitude\']" ).val()==""){
						TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_latitude\']" ).val(lat);
					}
					if(TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_longitude\']" ).val()==""){
						TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_longitude\']" ).val(lng);
					}																
				} else {
					//convert location into longitude and latitude
					geocoder.geocode({						
						address: query
					}, function(locResult) {
						lat = locResult[0].geometry.location.lat();
						lng = locResult[0].geometry.location.lng();
						gmegLatLng = 		new google.maps.LatLng(lat,lng);
						gmegMap = 			new google.maps.Map(
												document.getElementById("gmeg_map_canvas"),
												{zoom:15,center:gmegLatLng,mapTypeId:google.maps.MapTypeId.ROADMAP}
											);
						gmegMarker = 		new google.maps.Marker(
												{map:gmegMap,position:gmegLatLng}
											);
						gmegInfoWindow = 	new google.maps.InfoWindow(
												{content: title}
											);
						gmegInfoWindow.open(gmegMap,gmegMarker);
						if(TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_latitude\']" ).val()==""){
							TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_latitude\']" ).val(lat);
						}
						if(TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_longitude\']" ).val()==""){
							TYPO3.jQuery( "input[name*=\'moox_news_geoinfo_longitude\']" ).val(lng);
						}										
					});
				}
				
			}			
		</script>';
		
		if(
			($PA['row']['moox_news_geoinfo_latitude']!="" && $PA['row']['moox_news_geoinfo_longitude']!="") || 
			($PA['row']['moox_news_geoinfo_street']!="" && ($PA['row']['moox_news_geoinfo_zip']!="" || $PA['row']['moox_news_geoinfo_city']!=""))
		)
		{
			
			$map .= '
			<script type="text/javascript" >				
				google.maps.event.addDomListener(window,"load",gmegInitializeMap);
			</script>';
		}
        return $formField.$map;		
	}
}