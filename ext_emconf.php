<?php
########################################################################
# Extension Manager/Repository config file for ext "moox_news_geoinfo".
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
    'title' => 'MOOX news geoinfo',
    'description' => 'Erweiterung von MOOX-News mit Feldern zur Geolokalisierung',
    'category' => 'plugin',
    'author' => 'Dominic Martin',
    'author_email' => 'dm@dcn.de',
    'shy' => '',
    'dependencies' => 'moox_news',
    'conflicts' => '',
    'priority' => 'bottom',
    'module' => '',
    'state' => 'experimental',
    'internal' => '',
    'uploadfolder' => 0,
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 1,
    'lockType' => '',
    'author_company' => 'DCN GmbH',
    'version' => '0.0.1',
    'constraints' => array(
        'depends' => array(
            'typo3' => '6.2.0-6.2.99',
            'moox_news' => '0.9.0-0.9.99',
        ),
        'conflicts' => array(),
        'suggests' => array(),
    )
);
