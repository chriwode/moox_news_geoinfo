<?php
defined('TYPO3_MODE') or die();

// set default language file as ll-reference
$extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_news_geoinfo']);

// set default language file as ll-reference
$ll = 'LLL:EXT:moox_news_geoinfo/Resources/Private/Language/locallang.xlf:';

// add new news field group
$GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['fieldGroups']['venue'] = array(
	'label' => $ll.'field_group.venue',
	'fields' => 'moox_news_geoinfo_venue,moox_news_geoinfo_street,moox_news_geoinfo_zip,moox_news_geoinfo_city,moox_news_geoinfo_country',
);

// set list view field for this element type
$GLOBALS['TCA']['tx_mooxnews_domain_model_news']['listViewFields']['moox_news_geoinfo'] = "moox_news_geoinfo_zip";
// set search fields this element type (define mooxNewsExtenderQuery to generate )
$GLOBALS['TCA']['tx_mooxnews_domain_model_news']['listViewSearchFields']['moox_news_geoinfo']['default'] = "titel,moox_news_geoinfo_zip";
// set search fields for custom query demand (has to be defined in NewsDemand)
$GLOBALS['TCA']['tx_mooxnews_domain_model_news']['listViewSearchFields']['moox_news_geoinfo']['moox_news_geoinfo_query'] = "moox_news_geoinfo_zip";
// extend default search fields
$GLOBALS['TCA']['tx_mooxnews_domain_model_news']['listViewSearchFields']['moox_news']['default'] .= ",moox_news_geoinfo_zip";


// set tca array of new moox_news elements
$tx_mooxnewsgeoinfo_domain_model_news = array(
    'moox_news_geoinfo_venue' => array(
        'exclude' => 1,
        'l10n_mode' => 'mergeIfNotBlank',
        'label' => $ll.'tx_mooxnewsgeoinfo_domain_model_news.venue',
        'config' => array(
            'type' => 'input',
            'size' => 40,
        ),
        // this is needed within moox_news extension to handle extended field in backend view
        'extkey' => 'moox_news_geoinfo',
        // set this to "1" if you want to show the field in backend preview
        'addToMooxNewsBackendPreview' => Array (
			// set this to text, date, datetime or currency
			'type' => 'text',
		),
		// special moox configuration		
		'moox' => array(
			'extkey' => 'moox_news_geoinfo',
			'variant' => 'moox_news',
			'plugins' => array(
				'mooxnewsfrontend' => array(
					'add','edit','list','detail'
				),
			),
			'sortable' => 1,
		),
    ),
    'moox_news_geoinfo_street' => array(
        'exclude' => 1,
        'l10n_mode' => 'mergeIfNotBlank',
        'label' => $ll.'tx_mooxnewsgeoinfo_domain_model_news.street',
        'config' => array(
            'type' => 'input',
            'size' => 40,
        ),
        // this is needed within moox_news extension to handle extended field in backend view
        'extkey' => 'moox_news_geoinfo',
        // set this to "1" if you want to show the field in backend preview
        'addToMooxNewsBackendPreview' => Array (
			// set this to text, date, datetime or currency
			'type' => 'text',
		),
		// special moox configuration		
		'moox' => array(
			'extkey' => 'moox_news_geoinfo',
			'variant' => 'moox_news',
			'plugins' => array(
				'mooxnewsfrontend' => array(
					'add','edit','list','detail'
				),
			),
			'sortable' => 1,
		),
    ),
    'moox_news_geoinfo_zip' => array(
        'exclude' => 1,
        'l10n_mode' => 'mergeIfNotBlank',
        'label' => $ll.'tx_mooxnewsgeoinfo_domain_model_news.zip',
        'config' => array(
            'type' => 'input',
            'eval' => 'num',
            'size' => 5,
        ),
        // this is needed within moox_news extension to handle extended field in backend view
        'extkey' => 'moox_news_geoinfo',
		// set this to "1" if you want to use the field as a filterable value in frontend
		'addToMooxNewsFrontendDemand' => array(
			'operator' => 'startsWith',
		),
		// set this to "1" if you want to show the field as a filter in frontend
		'addToMooxNewsFrontendFilter' => 1,
		// set this to "1" if you want to show the field field in frontend list view
		'addToMooxNewsFrontendListView' => Array (
			// set this to text, date, datetime or currency
			'type' => 'text',
		),
		// set this to "1" if you want to show the field in plugin sorting field selection
		'addToMooxNewsFrontendSorting' => Array (
			// set this to text, date, datetime or currency
			'additionalSorting' => 'text ASC',
		),
        // set this to "1" if you want to show the field in backend preview
        'addToMooxNewsBackendPreview' => 0,
		// special moox configuration		
		'moox' => array(
			'extkey' => 'moox_news_geoinfo',
			'variant' => 'moox_news',
			'plugins' => array(
				'mooxnewsfrontend' => array(
					'add','edit','list','detail'
				),
			),
			'sortable' => 1,
		),
    ),
    'moox_news_geoinfo_city' => array(
        'exclude' => 1,
        'l10n_mode' => 'mergeIfNotBlank',
        'label' => $ll.'tx_mooxnewsgeoinfo_domain_model_news.city',
        'config' => array(
            'type' => 'input',
            'size' => 40
        ),
        // this is needed within moox_news extension to handle extended field in backend view
        'extkey' => 'moox_news_geoinfo',
        // set this to "1" if you want to show the field in backend preview
        'addToMooxNewsBackendPreview' => Array (
			// set this to text, date, datetime or currency
			'type' => 'text',
		),
		// special moox configuration		
		'moox' => array(
			'extkey' => 'moox_news_geoinfo',
			'variant' => 'moox_news',
			'plugins' => array(
				'mooxnewsfrontend' => array(
					'add','edit','list','detail'
				),
			),
			'sortable' => 1,
		),
    ),
    'moox_news_geoinfo_country' => array(
        'exclude' => 1,
        'l10n_mode' => 'mergeIfNotBlank',
        'label' => $ll.'tx_mooxnewsgeoinfo_domain_model_news.country',
        'config' => array(
            'type' => 'input',
            'size' => 40,
            'form_type' => 'user',
            'userFunc' => 'EXT:moox_news_geoinfo/Classes/Hooks/CustomTcaFunc.php:&Tx_MooxNewsGeoinfo_Hooks_CustomTcaFunc->generateTcaCountryField'
        ),
        // this is needed within moox_news extension to handle extended field in backend view
        'extkey' => 'moox_news_geoinfo',
        // set this to "1" if you want to show the field in backend preview
        'addToMooxNewsBackendPreview' => Array (
			// set this to text, date, datetime or currency
			'type' => 'text',
		),
		// special moox configuration		
		'moox' => array(
			'extkey' => 'moox_news_geoinfo',
			'variant' => 'moox_news',
			'plugins' => array(
				'mooxnewsfrontend' => array(
					'add','edit','list','detail'
				),
			),
			'sortable' => 1,
		),
    ),
    'moox_news_geoinfo_latitude' => array(
        'exclude' => 1,
        'l10n_mode' => 'mergeIfNotBlank',
        'label' => $ll.'tx_mooxnewsgeoinfo_domain_model_news.latitude',
        'config' => array(
            'type' => 'input',
            'size' => 10,
        ),
        // this is needed within moox_news extension to handle extended field in backend view
        'extkey' => 'moox_news_geoinfo',
        // set this to "1" if you want to show the field in backend preview
        'addToMooxNewsBackendPreview' => Array (
			// set this to text, date, datetime or currency
			'type' => 'text',
		),
    ),
    'moox_news_geoinfo_longitude' => array(
        'exclude' => 1,
        'l10n_mode' => 'mergeIfNotBlank',
        'label' => $ll.'tx_mooxnewsgeoinfo_domain_model_news.longitude',
        'config' => array(
            'type' => 'input',
            'size' => 10,
        ),
        // this is needed within moox_news extension to handle extended field in backend view
        'extkey' => 'moox_news_geoinfo',
        // set this to "1" if you want to show the field in backend preview
        'addToMooxNewsBackendPreview' => Array (
			// set this to text, date, datetime or currency
			'type' => 'text',
		),
    ),
    'moox_news_geoinfo_elevation' => array(
        'exclude' => 1,
        'l10n_mode' => 'mergeIfNotBlank',
        'label' => $ll.'tx_mooxnewsgeoinfo_domain_model_news.elevation',
        'config' => array(
            'type' => 'input',
            'size' => 5,
        ),
        // this is needed within moox_news extension to handle extended field in backend view
        'extkey' => 'moox_news_geoinfo',
        // set this to "1" if you want to show the field in backend preview
        'addToMooxNewsBackendPreview' => 0
    ),
    'moox_news_geoinfo_lat_lng_query' => array(
        'label' => 'moox_news_geoinfo_lat_lng_query',
        'config' => array(
            'type' => 'passthrough',
        )
    ),
    'moox_news_geoinfo_address_query' => array(
        'label' => 'moox_news_geoinfo_address_query',
        'config' => array(
            'type' => 'passthrough',
        )
    ),
);

// extend moox_news tca with new fields from this extension
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_mooxnews_domain_model_news', $tx_mooxnewsgeoinfo_domain_model_news);

// place new fields in backend form - optionally generate new tab for extended fields
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tx_mooxnews_domain_model_news', '--div--;'.(($extConf['overwriteTabName']!="")?$extConf['overwriteTabName']:$ll.'tx_mooxnewsgeoinfo_domain_model_news.tabs.geoinfo').',moox_news_geoinfo_venue,moox_news_geoinfo_street,moox_news_geoinfo_zip,moox_news_geoinfo_city,moox_news_geoinfo_country,moox_news_geoinfo_latitude,moox_news_geoinfo_longitude,moox_news_geoinfo_elevation', '', 'after:path_segment');

?>