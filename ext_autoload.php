<?php
$extensionClassesPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('moox_news_geoinfo') . 'Classes/';

$default = array(
	'Tx_MooxNewsGeoinfo_Domain_Model_News' => $extensionClassesPath . 'Domain/Model/News.php',
	'Tx_MooxNewsGeoinfo_Domain_Model_Dto_NewsDemand' => $extensionClassesPath . 'Domain/Model/Dto/NewsDemand.php',
	'Tx_MooxNewsGeoinfo_Hooks_Tcemain' => $extensionClassesPath . 'Hooks/Tcemain.php',
    'Tx_MooxNewsGeoinfo_Hooks_CustomTcaFunc' => $extensionClassesPath . 'Hooks/CustomTcaFunc.php',
);
