## MOOX News-Extension moox_news_geoinfo
=========

### TYPO3-Extension moox_news_geoinfo

This Ext. is fork of [moox_news_geoinfo](https://github.com/typo3-moox/moox_news_geoinfo).

MOOX News Geo Info adds lat, lon and alt as well as loc-fields to MOOX News. Easy to create location-based news-elements with latitude, longitude, altitude and location informations. Easily create responsive Maps integration with some lines of fluid.


### Visit http://www.moox.org

Issues: https://github.com/dcngmbh/moox_core/issues

Repository: http://typo3.org/extensions/repository/view/moox_news_geoinfo

Watch Video: https://www.youtube.com/watch?v=3dL-VImR2x8

Sourceforge: https://sourceforge.net/projects/moox-typo3-bootstrap/
